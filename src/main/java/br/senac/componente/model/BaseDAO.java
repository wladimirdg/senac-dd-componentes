package br.senac.componente.model;
public interface BaseDAO<A,B> {
    public A getPorId(B id);
    public boolean excluir(B id);
    public boolean alterar(A objeto);
    public B inserir(A objeto);
}

package br.senac.dd.exception;
public class ParametrosInvalidosExeception extends RuntimeException {
    private Integer numeroErro;

    public ParametrosInvalidosExeception() {
    }

    public ParametrosInvalidosExeception(Integer numeroErro) {
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosExeception(Integer numeroErro, String message) {
        super(message);
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosExeception(Integer numeroErro, String message, Throwable cause) {
        super(message, cause);
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosExeception(Integer numeroErro, Throwable cause) {
        super(cause);
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosExeception(Integer numeroErro, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.numeroErro = numeroErro;
    }

    public ParametrosInvalidosExeception(String message) {
        super(message);
    }

    public ParametrosInvalidosExeception(String message, Throwable cause) {
        super(message, cause);
    }

    public ParametrosInvalidosExeception(Throwable cause) {
        super(cause);
    }

    public ParametrosInvalidosExeception(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public Integer getNumeroErro() {
        return numeroErro;
    }

    public void setNumeroErro(Integer numeroErro) {
        this.numeroErro = numeroErro;
    }
    
}

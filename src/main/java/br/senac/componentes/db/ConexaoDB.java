package br.senac.componentes.db;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class ConexaoDB {
    public static void main(String[] args){
        ConexaoDB conDB = new ConexaoDB();
        System.out.println(conDB.getConnection());
    }
    private static Connection conn = null;
    public Connection getConnection(){
        try{
            if(conn!=null && conn.isClosed() ==  false){
               return conn;
            }
        } catch (SQLException ex){
            //Não fazer nada, tenta conectar novamente.
        }
        String porta = "3306";
        String servidor = "localhost";
        String usuario = "root";
        String senha = "";
        String bancoDados = "projeto";
        String url = "jdbc:mysql://"+servidor+":"+porta+"/"+bancoDados;
        try{
        conn = DriverManager.getConnection(url, usuario, porta);
        } catch (SQLException ex){
            throw new RuntimeException("Problemas na conexão com o Banco de Dados, "+"Consulte o suporte"+ex.getMessage(),ex);
        }
        return conn;
    }
}
